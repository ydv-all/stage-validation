#!/bin/bash 

repo=$1
stage=$2
startdate=$3
enddate=$4
st1=$(echo $startdate | cut -d "-" -f 1)
st2=$(echo $startdate | cut -d "-" -f 2)
en1=$(echo $enddate | cut -d "-" -f 1)
en2=$(echo $enddate | cut -d "-" -f 2)
rm -rf key.json
curl --location --request GET "https://rc3jxwm5t6.execute-api.us-east-1.amazonaws.com/dev/v1/deployment/getDeployments?limit=40&repo=${repo}&stage=${stage}" --header 'x-api-key:  DQrZmjJprR4mqmgWJkjfG59h798e0PPxwu2NCxj0' --header 'Content-Type: application/json' | jq '[ .data[] | { deployedBy, stage, repo, commitId, commitTime, apiName, serviceName, source, branch, tag, status, client } ]' >data.json
echo "$st1-$st2 *** $en1-$en2"

if [[ "$st1" == "$en1" ]]; then
     echo "printing same month data"
     for i in $(eval echo {$st2..31}); do
          if [[ $i -le 9 ]]; then
               echo $i
               jq ".[]| select(.commitTime | try contains(\"2021-$st1-$i\"))" data.json | jq -c . >>key.json
          else
               jq ".[]| select(.commitTime | try contains(\"2021-$st1-$i\"))" data.json | jq -c . >>key.json
          fi
     done

else

     echo "printing different months data"
     for j in $(eval echo {$st2..31}); do
          if [[ $j -le 9 ]]; then
               jq ".[]| select(.commitTime | try contains(\"2021-$st1-0$j\"))" data.json | jq -c . >>key.json
          else
               jq ".[]| select(.commitTime | try contains(\"2021-$st1-$j\"))" data.json | jq -c . >>key.json
          fi
     done
     echo "second month data"
     for k in $(eval echo {1..$en2}); do
          if [[ $k -le 9 ]]; then
               jq ".[]| select(.commitTime | try contains(\"2021-$en1-0$k\"))" data.json | jq -c . >>key.json
          else
               jq ".[]| select(.commitTime | try contains(\"2021-$en1-$k\"))" data.json | jq -c . >>key.json
          fi

     done
fi
